Utils = require './utils'

class Venue

  constructor: (@api_key) ->
    @calendar = @_calendar

  _calendar: (options, callback) ->
    url = "/venues/#{options.id}/calendar.json"
    params =
      apikey: @api_key
      per_page: 50
      page: 1
      skResultKey: "event"

    if typeof options is "function"
      callback = options
      options = {}

    Utils.getClean url, Utils.merge(params,options), callback

module.exports = Venue
