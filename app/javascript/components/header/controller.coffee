Scraper.controller "HeaderController", ["$scope", "$rootScope", "EventsService", "$element", "$timeout", ($scope, $rootScope, EventsService, $element, $timeout)->

  $scope.eventResults = []

  getResults = ->
    if $scope.query? and $scope.query isnt ""
      EventsService.search query: $scope.query, (events)->
        $timeout ->
          $scope.eventResults = events
          console.log "", $scope.eventResults.length

          do applyResults

  applyResults = ->
    if $scope.query isnt ""
      $scope.$broadcastAll "app:search:show", $scope.eventResults, $scope.query
    else
      $scope.$broadcastAll "app:search:clear"


  $scope.startSearch = ->
    $scope.$emit "app:search:start"
    do getResults

  $scope.stopSearch = ->
    $scope.$emit "app:search:stop"

  $scope.$watch "query", ->
    do getResults

  $scope.$on "app:search:clear", ->
    $scope.query = ""

  $scope.$on "keypress", (ev, event)=>
    if event.which is 13 and $(":focus").is ".filter-input"
      $element.find(".filter-input").trigger "blur"

]