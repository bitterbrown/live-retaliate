Sync  = require "sync"
db    = require "../database/connection"

class LiveModel

  collectionName  : ""
  attributes      : {}
  key             : "id"
  titleKey        : "displayName"
  dirty           : no
  options:
    sync : no

  constructor: (attributes, options)->
    throw Error "No collection given"   if @collectionName is ""
    throw Error "Model need a key"      if @key is ""

    @collection = db[@collectionName]
    @attributes = @parse attributes
    @options = options if options?

  getQuery: ->
    q = {}
    q[@key] = @attributes[@key]
    q

  matchValues: (a, b)->
    try
      a = a.replace new RegExp("[:'><\?\(\)\+\=\"]+","gmi"), ""
      a = a.trim()

      b = b.replace new RegExp("[:'><\?\(\)\+\=\"]+","gmi"), ""
      b = b.trim()
    catch
      return false

    `a.toLowerCase() == b.toLowerCase()`

  get: (name)->
    @attributes[name]

  set: (name, value)->
    unless @dirty is true
      @dirty = @attributes[name] isnt value

    @attributes[name] = null if not @attributes[name]?
    @attributes[name] = value

  parse: (attributes)->
    delete attributes._id
    attributes.updated_at = attributes.updated_at || new Date()

    attributes

  fetch: (next)->
    if @get(@key)?
      @collection.findOne do @getQuery, (err, data)=>
        if err isnt null then return console.error err.err

        if data?
          @attributes = @parse data
          return next null, data
        else
          console.log "#{@collectionName}:MODEL_NOT_FOUND", do @getQuery
          next null, false
    else
      console.error "#{@collectionName}:MODEL:NO_KEY_FOUND", do @getQuery

  delete: (next)->
    return console.log "No key attribute (#{@key}) found" if not @key? or not @attributes[@key]?

    @collection.remove do @getQuery, (err, result)=>
      if err isnt null then return console.error err.err

      console.log "DELETE::#{@collectionName}::#{@attributes[@titleKey]}"
      next null, result

  update: (next)->
    delete @attributes._id

    @attributes.updated_at = new Date()
    @collection.update do @getQuery, {$set: @attributes}, (err, result)=>
      if err isnt null then return console.error err.err

      console.log "UPDATE::#{@collectionName}::#{@attributes[@titleKey]}"
      next null, result if next?

  insert: (next)->
    return console.log "No key attribute (#{@key}) found" if not @key? or not @attributes[@key]?

    @attributes.inserted_at = new Date()
    @collection.insert @attributes, (err, result)=>
      if err isnt null
        console.error err.err
        next null, err if next?
      else
        console.log "INSERT::#{@collectionName}::#{@attributes[@titleKey]}"
        next null, result if next?

        @afterInsert.call @, result

  afterInsert: ->
    true



module.exports = LiveModel