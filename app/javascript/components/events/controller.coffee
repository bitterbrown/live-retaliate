Scraper.controller "EventsController", ["$scope", "$rootScope", "EventsService", "BandsService", "$timeout", "CONFIG", ($scope, $rootScope, EventsService, BandsService, $timeout, CONFIG) ->

  isLoading   = false
  dateLimit   = moment hours: 0, minutes: 0
  activeEvent = -1

  updateRelatedEvents = (band)->
    for event,i in $scope.events
      if event.performance? and (_band for _band in event.performance when _band.name is band.name).length > 0
        $scope.events[i] = EventsService.get id: event.id

  highlightRequiredDate = (method)->
    $active = $(".event-item:eq(#{activeEvent})")

    $header = $active["#{method}All"]("header[event-index!=#{$active.attr("event-index")}]:first")

    if $header.size() is 0 # Don't have any header to show, let's add more chunks
      @loadMoreEvents =>
        $timeout =>
          highlightRequiredDate method
        , 0

    else # Next date header found
      activeEvent = new Number $header.attr("event-index")
      $scope.$broadcastAll "goto:event", 0

  progressiveLoad = (index, events, next)->
    if index < events.length
      $timeout ->
        $scope.events.push events[index]
        index++
        progressiveLoad index, events, next
      , 50

    else
      do next if next?

  angular.extend $scope,
    events            : do EventsService.getNextEvents
    showCrap          : false
    isPhone           : Scraper.config.isPhone
    noMoreEvents      : false
    searchQuery       : ""
    staticHost        : CONFIG.LiveStaticHost

    toggleCrapEvents: ->
      @showCrap = not @showCrap

    openEvent: (scope)->
      if scope.event.status isnt "cancelled"
        $scope.$broadcastAll "goto:event", scope.$index, true
        $scope.$broadcastAll "event:show", scope

        console.log "Open event", scope.event if CONFIG.ENV is "development"

    loadMoreEvents: (callback)->
      if isLoading is no and @noMoreEvents isnt true
        isLoading = yes

        dateLimit.add 1, "day"

        EventsService.getNextEvents
          date: dateLimit._d
        , (events)=>
          @noMoreEvents = events.length is 0

          if events.length > 0
            @events.push event for event in events

            $rootScope.$$phase || @$apply "events"
            isLoading = no

            do callback if callback?

        , (error)->
          do callback if callback?

  $scope.showCrapEvents = (event)->
    if $scope.showCrap is true then return true else return event.crap is false



  #---------------------------------------------------------------------------------------------------------------------

  $scope.$on "app:search:show", (ev, events, searchQuery)->
    $timeout ->
      $scope.events = events
      $scope.searchQuery = searchQuery

  $scope.$on "app:search:clear", ->
    $timeout ->
      $scope.events = do EventsService.getNextEvents
      $scope.searchQuery = ""

  $scope.$on "goto:event",
    (ev, dir, force)->
      return if activeEvent is $scope.events.length - 1 and dir is 1

      $(".event-item.active").removeClass "active"

      activeEvent += dir
      activeEvent = dir if force is true
      activeEvent = if activeEvent < 0 then 0 else activeEvent
      activeElem = $(".event-item:eq(#{activeEvent})")
      activeElem.addClass "active"

      $timeout ->
        if (activeElem.offset().top + activeElem.height()) > $("#events-list").height() - 40
          $("#events-list").stop().animate
            scrollTop: $("#events-list").scrollTop() + activeElem.position().top - $("#events-list").height()/2
            scrollLeft: $("#events-list").scrollLeft() + activeElem.position().left - $("#events-list").width()/2
          , 700

        if activeElem.offset().top < 0
          $("#events-list").stop().animate
            scrollTop: $("#events-list").scrollTop() + activeElem.position().top
            scrollLeft: $("#events-list").scrollLeft() + activeElem.position().left
          , 700
      , if $("html").hasClass "has-event" then 0 else 500


  $scope.$on "event:mark",
    (_event, property, scope)->
      if scope.event.performance.length is 1
        $scope.$emit "set:band", property, scope.event.performance[0]
      else
        $scope.$broadcastAll "show:modal",
          title   : "Mark as #{property}!"
          prop    : property
          buttons : scope.event.performance
          event   : scope.event

  $scope.$on "set:band",
    (_event, property, _band)->
      band = new BandsService _band
      band[property] = not _band[property]
      band.$update ->
        $scope.$broadcastAll "band:update", band
        updateRelatedEvents band

  $scope.$on "goto:date",
    (ev, dir)=>
      highlightRequiredDate if dir > 0 then "next" else "prev"

  $scope.$on "event:update", (ev, liveEvent)->
    for event,i in $scope.events when event.id is liveEvent.id
      $scope.events[i] = liveEvent

]