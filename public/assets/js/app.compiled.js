var Scraper;

Scraper = angular.module("Scraper", ["ngResource", "ngRoute"]);

Scraper.config([
  '$routeProvider', function($routeProvider) {
    $routeProvider.when("/", {
      templateUrl: "/templates/components/main/view.html"
    });
    return $routeProvider.when("/stats", {
      controller: "StatsController",
      templateUrl: "/templates/components/stats/view.html"
    });
  }
]);

Scraper.constant("CONFIG", {
  ENV: LiveEnvironment,
  API_URI: LiveApiUri,
  LiveStaticHost: LiveStaticHost
});

Scraper.run(function() {
  return Scraper.config = {
    isPhone: $("body").width() < 600
  };
});

Scraper.directive("bandButton", [
  "$rootScope", function($rootScope) {
    return {
      restrict: "EA",
      link: function(scope, $el, attrs) {
        scope.$on("band:update", function(ev, band) {
          if (scope.button.name === band.name) {
            return scope.button = band;
          }
        });
        return scope.$watch("button", function() {
          return $el.attr("class", "").addClass("button " + (scope.button.awesome ? "is-awesome" : void 0) + " " + (scope.button.crap ? "is-crap" : void 0)).html(scope.button.name).on("click", (function(_this) {
            return function() {
              return $rootScope.$broadcast("set:band", attrs.property, scope.button);
            };
          })(this));
        });
      }
    };
  }
]);

Scraper.directive("infiniteLoad", function() {
  return {
    restrict: 'A',
    link: function(scope, element) {
      return element.on("scroll", (function(_this) {
        return function() {
          if (scope.searchQuery === "") {
            if (!Scraper.config.isPhone || (Scraper.config.isPhone === true && $("html").is(":not(.has-event)"))) {
              if (element.children(":last").prev().offset().top - element.height() < 400) {
                return scope.loadMoreEvents();
              }
            } else if (Scraper.config.isPhone === true && $("html").is(".has-event")) {
              if (element.children(":last").prev().offset().left < $("body").width()) {
                return scope.loadMoreEvents();
              }
            }
          }
        };
      })(this));
    }
  };
});

Scraper.directive("modalContainer", function() {
  return {
    restrict: 'E',
    controller: "ModalController",
    templateUrl: "/templates/components/modals/view.html"
  };
});

Scraper.directive("picturesRodeo", function($timeout) {
  return {
    restrict: 'A',
    link: function(scope, element, attr) {
      var animate, intId;
      intId = 0;
      animate = function() {
        if (element.find(".event-picture").size() === 0) {
          return clearInterval(intId);
        }
        if (element.find(".event-picture").size() > 1) {
          return intId = setInterval((function(_this) {
            return function() {
              element.find(".event-picture:not(.done):last").addClass("done");
              if (element.find(".event-picture:not(.done)").size() === 0) {
                return element.find(".event-picture").removeClass("done");
              }
            };
          })(this), Math.random(0, 1) * 1500 + 3500);
        }
      };
      if (attr.picturesRodeo !== "") {
        return scope.$watch(attr.picturesRodeo, function() {
          clearInterval(intId);
          return animate();
        });
      } else {
        return $timeout((function(_this) {
          return function() {
            return animate();
          };
        })(this));
      }
    }
  };
});

Scraper.filter("fullEventDate", function() {
  return function(input) {
    var date, format;
    if (input != null) {
      format = "dddd - MMMM Do - YYYY" + (input.datetime != null ? ", h:mm a" : "");
      date = input.datetime != null ? input.datetime : input.date;
      return moment(date).format(format);
    } else {
      return false;
    }
  };
});

Scraper.filter("removeVenue", function() {
  return function(input) {
    if (input.indexOf("at") === -1) {
      return input;
    }
    return input.substring(0, input.lastIndexOf("at"));
  };
});

Scraper.filter("stripQuotes", function() {
  return function(input) {
    return input.split('"').join("");
  };
});

Scraper.filter("eventListDate", function($sce) {
  return function(input) {
    if (!(input == null)) {
      input = input.replace(/(\,+)/g, "");
      input = input.replace(/([a-zA-Z0-9\-\_]+)\s/g, "<span>$1</span>");
      return $sce.trustAsHtml(input);
    }
  };
});

Scraper.factory("BandsService", [
  "$resource", "CONFIG", function($resource, CONFIG) {
    return $resource("" + CONFIG.API_URI + "/api/bands/:name", {
      name: "@name"
    }, {
      update: {
        method: "PUT"
      }
    });
  }
]);

Scraper.factory("EventsService", [
  '$resource', "CONFIG", function($resource, CONFIG) {
    return $resource("" + CONFIG.API_URI + "/api/events/:id", {
      id: "@id"
    }, {
      update: {
        method: "PUT"
      },
      getNextEvents: {
        url: "" + CONFIG.API_URI + "/api/events/bydate",
        isArray: true,
        params: {
          date: moment({
            hours: 0,
            minutes: 0
          })._d
        }
      },
      getTracks: {
        url: "" + CONFIG.API_URI + "/api/events/:id/tracks",
        method: "GET",
        isArray: true
      },
      getRelatedArtists: {
        url: "" + CONFIG.API_URI + "/api/events/:id/related-artists",
        method: "GET"
      },
      search: {
        url: "" + CONFIG.API_URI + "/api/events/search/:query",
        method: "GET",
        isArray: true,
        params: {
          query: "@query"
        }
      },
      searchCount: {
        url: "" + CONFIG.API_URI + "/api/events/search/:query",
        method: "GET",
        isArray: false,
        params: {
          query: "@query"
        }
      }
    });
  }
]);

Scraper.controller("EventController", [
  "$scope", "$rootScope", "$http", "EventsService", function($scope, $rootScope, $http, EventsService) {
    var getTracksUri;
    $scope.activeEvent = false;
    getTracksUri = function(tracks) {
      var i, track;
      if (tracks.length > 40) {
        tracks = (function() {
          var _i, _len, _results;
          _results = [];
          for (i = _i = 0, _len = tracks.length; _i < _len; i = ++_i) {
            track = tracks[i];
            if (i % (parseInt(tracks.length / 40)) === 0) {
              _results.push(track);
            }
          }
          return _results;
        })();
      }
      return (function() {
        var _i, _len, _results;
        _results = [];
        for (_i = 0, _len = tracks.length; _i < _len; _i++) {
          track = tracks[_i];
          _results.push(track.replace("spotify:track:", ""));
        }
        return _results;
      })();
    };
    return $scope.$on("event:show", function(ev, scope) {
      angular.extend($scope, {
        activeEvent: scope.event,
        relatedArtists: false
      });
      EventsService.getTracks({
        id: scope.event.id
      }, function(tracks) {
        tracks = getTracksUri(tracks);
        $scope.activeEvent.tracks = tracks;
        if (tracks.length > 0) {
          return window.player.location.href = "https://embed.spotify.com/?&uri=spotify:trackset:Listen:" + (tracks.join(",")) + "&view=list";
        }
      });
      EventsService.getRelatedArtists({
        id: scope.event.id
      }, function(related) {
        var band;
        if (((function() {
          var _results;
          _results = [];
          for (band in related) {
            if (related[band].length > 0) {
              _results.push(band);
            }
          }
          return _results;
        })()).length > 0) {
          return $scope.relatedArtists = related;
        }
      });
      return $rootScope.$$phase || $scope.$apply("activeEvent");
    });
  }
]);

Scraper.controller("EventsController", [
  "$scope", "$rootScope", "EventsService", "BandsService", "$timeout", "CONFIG", function($scope, $rootScope, EventsService, BandsService, $timeout, CONFIG) {
    var activeEvent, dateLimit, highlightRequiredDate, isLoading, progressiveLoad, updateRelatedEvents;
    isLoading = false;
    dateLimit = moment({
      hours: 0,
      minutes: 0
    });
    activeEvent = -1;
    updateRelatedEvents = function(band) {
      var event, i, _band, _i, _len, _ref, _results;
      _ref = $scope.events;
      _results = [];
      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
        event = _ref[i];
        if ((event.performance != null) && ((function() {
          var _j, _len1, _ref1, _results1;
          _ref1 = event.performance;
          _results1 = [];
          for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
            _band = _ref1[_j];
            if (_band.name === band.name) {
              _results1.push(_band);
            }
          }
          return _results1;
        })()).length > 0) {
          _results.push($scope.events[i] = EventsService.get({
            id: event.id
          }));
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };
    highlightRequiredDate = function(method) {
      var $active, $header;
      $active = $(".event-item:eq(" + activeEvent + ")");
      $header = $active["" + method + "All"]("header[event-index!=" + ($active.attr("event-index")) + "]:first");
      if ($header.size() === 0) {
        return this.loadMoreEvents((function(_this) {
          return function() {
            return $timeout(function() {
              return highlightRequiredDate(method);
            }, 0);
          };
        })(this));
      } else {
        activeEvent = new Number($header.attr("event-index"));
        return $scope.$broadcastAll("goto:event", 0);
      }
    };
    progressiveLoad = function(index, events, next) {
      if (index < events.length) {
        return $timeout(function() {
          $scope.events.push(events[index]);
          index++;
          return progressiveLoad(index, events, next);
        }, 50);
      } else {
        if (next != null) {
          return next();
        }
      }
    };
    angular.extend($scope, {
      events: EventsService.getNextEvents(),
      showCrap: false,
      isPhone: Scraper.config.isPhone,
      noMoreEvents: false,
      searchQuery: "",
      staticHost: CONFIG.LiveStaticHost,
      toggleCrapEvents: function() {
        return this.showCrap = !this.showCrap;
      },
      openEvent: function(scope) {
        if (scope.event.status !== "cancelled") {
          $scope.$broadcastAll("goto:event", scope.$index, true);
          $scope.$broadcastAll("event:show", scope);
          if (CONFIG.ENV === "development") {
            return console.log("Open event", scope.event);
          }
        }
      },
      loadMoreEvents: function(callback) {
        if (isLoading === false && this.noMoreEvents !== true) {
          isLoading = true;
          dateLimit.add(1, "day");
          return EventsService.getNextEvents({
            date: dateLimit._d
          }, (function(_this) {
            return function(events) {
              var event, _i, _len;
              _this.noMoreEvents = events.length === 0;
              if (events.length > 0) {
                for (_i = 0, _len = events.length; _i < _len; _i++) {
                  event = events[_i];
                  _this.events.push(event);
                }
                $rootScope.$$phase || _this.$apply("events");
                isLoading = false;
                if (callback != null) {
                  return callback();
                }
              }
            };
          })(this), function(error) {
            if (callback != null) {
              return callback();
            }
          });
        }
      }
    });
    $scope.showCrapEvents = function(event) {
      if ($scope.showCrap === true) {
        return true;
      } else {
        return event.crap === false;
      }
    };
    $scope.$on("app:search:show", function(ev, events, searchQuery) {
      return $timeout(function() {
        $scope.events = events;
        return $scope.searchQuery = searchQuery;
      });
    });
    $scope.$on("app:search:clear", function() {
      return $timeout(function() {
        $scope.events = EventsService.getNextEvents();
        return $scope.searchQuery = "";
      });
    });
    $scope.$on("goto:event", function(ev, dir, force) {
      var activeElem;
      if (activeEvent === $scope.events.length - 1 && dir === 1) {
        return;
      }
      $(".event-item.active").removeClass("active");
      activeEvent += dir;
      if (force === true) {
        activeEvent = dir;
      }
      activeEvent = activeEvent < 0 ? 0 : activeEvent;
      activeElem = $(".event-item:eq(" + activeEvent + ")");
      activeElem.addClass("active");
      return $timeout(function() {
        if ((activeElem.offset().top + activeElem.height()) > $("#events-list").height() - 40) {
          $("#events-list").stop().animate({
            scrollTop: $("#events-list").scrollTop() + activeElem.position().top - $("#events-list").height() / 2,
            scrollLeft: $("#events-list").scrollLeft() + activeElem.position().left - $("#events-list").width() / 2
          }, 700);
        }
        if (activeElem.offset().top < 0) {
          return $("#events-list").stop().animate({
            scrollTop: $("#events-list").scrollTop() + activeElem.position().top,
            scrollLeft: $("#events-list").scrollLeft() + activeElem.position().left
          }, 700);
        }
      }, $("html").hasClass("has-event") ? 0 : 500);
    });
    $scope.$on("event:mark", function(_event, property, scope) {
      if (scope.event.performance.length === 1) {
        return $scope.$emit("set:band", property, scope.event.performance[0]);
      } else {
        return $scope.$broadcastAll("show:modal", {
          title: "Mark as " + property + "!",
          prop: property,
          buttons: scope.event.performance,
          event: scope.event
        });
      }
    });
    $scope.$on("set:band", function(_event, property, _band) {
      var band;
      band = new BandsService(_band);
      band[property] = !_band[property];
      return band.$update(function() {
        $scope.$broadcastAll("band:update", band);
        return updateRelatedEvents(band);
      });
    });
    $scope.$on("goto:date", (function(_this) {
      return function(ev, dir) {
        return highlightRequiredDate(dir > 0 ? "next" : "prev");
      };
    })(this));
    return $scope.$on("event:update", function(ev, liveEvent) {
      var event, i, _i, _len, _ref, _results;
      _ref = $scope.events;
      _results = [];
      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
        event = _ref[i];
        if (event.id === liveEvent.id) {
          _results.push($scope.events[i] = liveEvent);
        }
      }
      return _results;
    });
  }
]);

Scraper.controller("HeaderController", [
  "$scope", "$rootScope", "EventsService", "$element", "$timeout", function($scope, $rootScope, EventsService, $element, $timeout) {
    var applyResults, getResults;
    $scope.eventResults = [];
    getResults = function() {
      if (($scope.query != null) && $scope.query !== "") {
        return EventsService.search({
          query: $scope.query
        }, function(events) {
          return $timeout(function() {
            $scope.eventResults = events;
            console.log("", $scope.eventResults.length);
            return applyResults();
          });
        });
      }
    };
    applyResults = function() {
      if ($scope.query !== "") {
        return $scope.$broadcastAll("app:search:show", $scope.eventResults, $scope.query);
      } else {
        return $scope.$broadcastAll("app:search:clear");
      }
    };
    $scope.startSearch = function() {
      $scope.$emit("app:search:start");
      return getResults();
    };
    $scope.stopSearch = function() {
      return $scope.$emit("app:search:stop");
    };
    $scope.$watch("query", function() {
      return getResults();
    });
    $scope.$on("app:search:clear", function() {
      return $scope.query = "";
    });
    return $scope.$on("keypress", (function(_this) {
      return function(ev, event) {
        if (event.which === 13 && $(":focus").is(".filter-input")) {
          return $element.find(".filter-input").trigger("blur");
        }
      };
    })(this));
  }
]);

Scraper.controller("Main", [
  "$scope", "$rootScope", function($scope, $rootScope) {
    $scope.$broadcastAll = function() {
      return $rootScope.$broadcast.apply($rootScope, arguments);
    };
    $scope.$on("event:show", function() {
      $("html").addClass("has-event");
      return false;
    });
    $scope.$on("event:hide", function() {
      $("html").removeClass("has-event");
      return false;
    });
    $scope.$on("app:search:start", function() {
      return $("html").removeClass("has-search-events").addClass("is-searching");
    });
    $scope.$on("app:search:stop", function() {
      $("html").removeClass("is-searching");
      return false;
    });
    $scope.$on("app:search:show", function() {
      return $("html").addClass("has-search-events");
    });
    $scope.$on("app:search:clear", function() {
      return $("html").removeClass("has-search-events");
    });
    return $(window).on("keydown", (function(_this) {
      return function(event) {
        var _ref, _ref1, _ref2;
        if (event.metaKey === false && event.ctrlKey === false && event.shiftKey === false && !$(":focus").is(".filter-input")) {
          if ((_ref = event.which) === 37 || _ref === 38 || _ref === 39 || _ref === 40) {
            event.stopPropagation();
            event.preventDefault();
            if ((_ref1 = event.which) === 38 || _ref1 === 40) {
              $rootScope.$broadcast("goto:event", event.which - 39);
            }
            if ((_ref2 = event.which) === 37 || _ref2 === 39) {
              $rootScope.$broadcast("goto:date", event.which - 38);
            }
          }
          if (event.which === 13) {
            $rootScope.$broadcast("event:show", angular.element(".event-item.active").scope());
          }
          $rootScope.$broadcast("keypress", event);
        }
        if ($("html").is(".is-searching")) {
          return $rootScope.$broadcast("keypress", event);
        }
      };
    })(this));
  }
]);

Scraper.controller("ModalController", [
  "$scope", "EventsService", "$rootScope", function($scope, EventsService, $rootScope) {
    this.component = $(".modal-component");
    this.resizeInt = false;
    angular.extend($scope, {
      show: false,
      close: function(event) {
        if ($(event.target).find(".modal-component").size() > 0) {
          return $scope.show = false;
        }
      },
      markEvent: function() {
        if (this.event != null) {
          this.event = new EventsService(this.event);
          this.event[this.prop] = !this.event[this.prop];
          return this.event.$update((function(_this) {
            return function() {
              $rootScope.$broadcast("event:update", _this.event);
              return _this.show = false;
            };
          })(this));
        }
      }
    });
    $scope.$on("show:modal", (function(_this) {
      return function(event, conf) {
        angular.extend($scope, conf);
        $scope.show = true;
        $scope.$$phase || $scope.$apply("show");
        _this.component.height(_this.component.find(".modal-content").outerHeight());
        return _this.component.width(_this.component.find(".modal-content").outerWidth());
      };
    })(this));
    $scope.$on("hide:modal", (function(_this) {
      return function() {
        $scope.show = false;
        return $scope.$$phase || $scope.$apply("show");
      };
    })(this));
    return $scope.$on("keypress", function(event, keyevent) {
      if (keyevent.which === 27) {
        $scope.show = false;
        return $scope.$$phase || $scope.$apply("show");
      }
    });
  }
]);

Scraper.controller("StatsController", [
  "$scope", "EventsService", "BandsService", "$http", function($scope, EventsService, BandsService, $http) {
    $scope.events = EventsService.get();
    $scope.bands = BandsService.get();
    $scope.showMissingImageList = false;
    $scope.showMissingTrackList = false;
    return $scope.fetchImage = function(scope, $event) {
      return $http.get("/api/events/" + scope.event.id + "/update").then(function(resp) {
        return $($event.currentTarget).html(resp.status === 200 ? "Request sent" : "Error!");
      });
    };
  }
]);

Scraper.filter("noimage", function() {
  return function(events, count) {
    var event, list;
    list = (function() {
      var _i, _len, _results;
      _results = [];
      for (_i = 0, _len = events.length; _i < _len; _i++) {
        event = events[_i];
        if (event.pictures.length === 0) {
          _results.push(event);
        }
      }
      return _results;
    })();
    if (count === true) {
      return list.length;
    } else {
      return list;
    }
  };
});

Scraper.filter("crap", function() {
  return function(events, count) {
    var event, list;
    list = (function() {
      var _i, _len, _results;
      _results = [];
      for (_i = 0, _len = events.length; _i < _len; _i++) {
        event = events[_i];
        if (event.crap === true) {
          _results.push(event);
        }
      }
      return _results;
    })();
    if (count === true) {
      return list.length;
    } else {
      return list;
    }
  };
});

Scraper.filter("awesome", function() {
  return function(events, count) {
    var event, list;
    list = (function() {
      var _i, _len, _results;
      _results = [];
      for (_i = 0, _len = events.length; _i < _len; _i++) {
        event = events[_i];
        if (event.awesome === true) {
          _results.push(event);
        }
      }
      return _results;
    })();
    if (count === true) {
      return list.length;
    } else {
      return list;
    }
  };
});

Scraper.filter("percent", function() {
  return function(number, list) {
    return parseInt((number / list.length) * 100, 10) + "%";
  };
});

//# sourceMappingURL=app.compiled.js.map
