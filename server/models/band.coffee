LiveModel       = require "./model"
LiveEvent       = require "./live-event"
SpotifyApi      = require "./../classes/api.spotify"
LastfmApi       = require "./../classes/api.lastfm"
SongkickApi     = require "./../classes/songkick/songkick"
Rest            = require "node-rest-client"
Sync            = require "sync"
ImageDownloader = require "./../classes/imageDownloader"
db              = require "../database/connection"


module.exports = do ->

  class BandModel extends LiveModel
    collectionName  : "bands"
    titleKey        : "name"
    key             : "name"

    imageSizeMatrix:
      small:
        width: 64, height: 52
      medium:
        width: 200, height: 163
      large:
        width: 640, height: 523
      extralarge:
        width: 1000, height: 816
      mega:
        width: 1000, height: 816

    getImages: (next)->
      Sync =>
        images = @getImagesInSpotify.sync @
        images = @getImagesInLastFm.sync @      if images is false
        images = @getImagesInSoundCloud.sync @  if images is false
        images = @getImagesInSongkick.sync @    if images is false

        @set "images", images if images isnt false
        next null, images

    getImagesInSpotify: (next)->
      console.log "BAND::#{@get "name"}::SEARCH_IMAGES::SPOTIFY"

      SpotifyApi.getArtistsByName @get("name"), (artists)=>
        for artist in artists
          if @matchValues(artist.name, @get("name"))
            if not @attributes.ids?.spotify? then @attributes.ids?.spotify = artist.id

            return next null, artist.images

        next null, false

    getImagesInLastFm: (next)->
      console.log "BAND::#{@get("name")}::SEARCH_IMAGES::LAST_FM"

      LastfmApi.artist.search {artist: encodeURI(@get("name")),limit: 5}, (err, result)=>
        if err is null and result["opensearch:totalResults"] > 0
          for artist in result.artistmatches.artist
            if @matchValues(artist.name, @get("name"))
              if not @attributes.ids?.lastfm? and artist.mbid isnt "" then @attributes.ids?.lastfm = artist.mbid

              images = []
              for image in artist.image
                images.push
                  url: image["#text"]
                  width: @imageSizeMatrix[image.size].width
                  height: @imageSizeMatrix[image.size].height

              return next null, images

          return next null, false
        else
          return next null, false

    getImagesInSoundCloud: (next)->
      console.log "BAND::#{@get("name")}::SEARCH_IMAGES::SOUND_CLOUD"

      params =
        q         : encodeURI(@get("name"))
        limit     : 1
        client_id : "b58f4b431ec3aff0bb0cf2e03dbf7850"

      client = new Rest.Client
      client.get "https://api.soundcloud.com/search/suggest", parameters: params, (data)=>
        if JSON.parse(data)?.suggestions?.length isnt 0
          artistID = JSON.parse(data).suggestions[0].id

          if not @attributes.ids?.soundcloud? then @attributes.ids?.soundcloud = artistID

          client2 = new Rest.Client
          client2.get "http://api.soundcloud.com/users/#{artistID}.json?client_id=b58f4b431ec3aff0bb0cf2e03dbf7850", (data)=>
            if JSON.parse(data)?.id?
              artist = JSON.parse(data)

              if @matchValues(artist.full_name, @get("name")) and artist.kind is "user"
                return next null, [
                  url: artist.avatar_url, width: @imageSizeMatrix["large"].width, height: @imageSizeMatrix["large"].height
                ]

              return next null, false

            else
              return next null, false

        else
          return next null, false

    getImagesInSongkick: (next)->
      console.log "BAND::#{@get("name")}::SEARCH_IMAGES::SONGKICK"

      next null, [
        url: "http://www2.sk-static.com/images/media/profile_images/artists/#{@get("id")}/huge_avatar"
        width: @imageSizeMatrix["large"].width
        height: @imageSizeMatrix["large"].height
      ]

    getThumbnail: (next)->
      if @get("images")?.length > 0 and @get("imageBest") is false
        image = false

        for _image in @get("images") when _image.url? and _image.url isnt ""
          image = _image if image is false or _image.width > image.width

        if image isnt false
          @set "imageBest", image
          Downloader = new ImageDownloader
          status = Downloader.processThumbnail.sync Downloader,
            src   : image.url
            dest  : "#{__dirname}/../../temp/"


          if status is true
            @set "thumbnail", Downloader.name
            next null

          else
            console.error "Error getting thumbnail"
            next null, "ERROR"
        else
          next null, false

      else
        next null, false

    getRelatedArtists: (next)->
      # Try songkick
      SongkickApi.getRelatedArtists @get("id"), (artists)=>
        if artists? and artists.length > 0
          @set "related", (artist.displayName for artist in artists)
          next null, (artist.displayName for artist in artists)
        else

          #Try lastfm
          LastfmApi.artist.getSimilar {artist: encodeURI(@get("name")), limit: 30}, (err, artists)=>
            if err is null and result["opensearch:totalResults"] > 0
              @set "related", (artist.name for artist in result.similarartists.artist)
              next null

            else

              # Try spotify
              if @get("ids")?.spotify?
                SpotifyApi.getRelatedArtists @get("ids").spotify, (artists)=>
                  if artists? and artists.length > 0
                    @set "related", (artist.name for artist in artists)

                  next null

              else
                next null


    getTracks: (next)->
      if @get("ids")?.spotify?
        SpotifyApi.getTracks @get("ids").spotify, (tracks)=>
          tracksUri = (track.uri for track in tracks when track.type is "track") || []

          @set "tracks", tracksUri
          next null, tracksUri

      else next null, []

    getOtherInfos: (next)->
      if @get("ids")?.spotify?
        SpotifyApi.getArtistsByName @get("name"), (artists)=>
          for artist in artists
            if @matchValues(artist.name, @get("name"))
              @set "genres", artist.genres
              @set "popularity", artist.popularity

              return next null

          next null

      else next null

    getAwesome: (next)->
      if @get("awesome") is no
        db.tracked.count name: @get("name"), (err, count)=>
          if err isnt null
            console.error err.err
            return next null

          @set "awesome", count > 0

          next null, count > 0

      else
        next null, yes

    getCoreAttributes: ->
      href        : @get("href")
      id          : @get("id")
      name        : @get("name")
      popularity  : @get("popularity")
      source      : @get("source")
      awesome     : @get("awesome")
      crap        : @get("crap")
      thumbnail   : @get("thumbnail")
      imageBest   : @get("imageBest")

    fetch: (next)->
      return console.error "BAND:FETCH:NO_KEY_FOUND" unless @get(@key)?

      if not @get("_id")?
        # Band not from DB, fetch it
        @collection.findOne name: @get("name"), (err, band)=>
          return console.error "#{@collectionName}:MODEL_NOT_FOUND", do @getQuery if err isnt null

          if band is null
            @insert next
          else
            @attributes = band
            return next null, @attributes if next?

      else
        next null, @attributes if next?

    emit: (next)->
      db.live_events.find("performance.name": new RegExp(@get("name"), "gi")).toArray (err, events)=>
        if err is null and events.length > 0
          Sync =>
            for _event in events
              event = new LiveEvent _event
              event.save.sync event

            next null, events if next?
        else
          next null if next?

    save: (next)->
      Sync =>
        @update.sync @
        @emit.sync @

        next null, @attributes

    insert: (next)->
      return console.log "No key attribute (#{@key}) found" if not @key? or not @attributes[@key]?

      Sync =>
        @getImages.sync @
        @getThumbnail.sync @
        @getRelatedArtists.sync @
        @getTracks.sync @
        @getOtherInfos.sync @
        @getAwesome.sync @

        @attributes.inserted_at = new Date()
        @collection.insert @attributes, (err)=>
          if err isnt null
            console.error err.err
            next null, err

          else
            console.log "INSERT::#{@collectionName}::#{@attributes[@titleKey]}"
            next null, @attributes

    parse: (data)->
      id        : data.artist?.id || data.id || null
      ids       : data.ids        || {}
      genres    : data.genres     || []
      href      : data.href       || data.uri || ""
      images    : data.images     || []
      name      : data.name       || data.displayName
      popularity: data.popularity || 0
      awesome   : data.awesome    || false
      crap      : data.crap       || false
      tracks    : data.tracks     || []
      thumbnail : data.thumbnail  || false
      imageBest : data.imageBest  || false
      related   : data.related    || []