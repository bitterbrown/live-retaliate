Scraper.controller "EventController", ["$scope", "$rootScope", "$http", "EventsService", ($scope, $rootScope, $http, EventsService)->

  $scope.activeEvent = false

  getTracksUri = (tracks)->
    if tracks.length > 40
      tracks = (track for track,i in tracks when i%(parseInt(tracks.length/40)) is 0)

    return (track.replace("spotify:track:", "") for track in tracks)

  $scope.$on "event:show", (ev, scope)->
    angular.extend $scope,
      activeEvent     : scope.event
      relatedArtists  : false

    EventsService.getTracks id: scope.event.id, (tracks)->
      tracks = getTracksUri tracks
      $scope.activeEvent.tracks = tracks

      if tracks.length > 0
        window.player.location.href = "https://embed.spotify.com/?&uri=spotify:trackset:Listen:#{tracks.join(",")}&view=list"

    EventsService.getRelatedArtists id: scope.event.id, (related)->
      if (band for band of related when related[band].length > 0).length > 0
        $scope.relatedArtists = related

    $rootScope.$$phase || $scope.$apply "activeEvent"

]
