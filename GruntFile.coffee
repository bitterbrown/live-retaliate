module.exports = (grunt) ->

  # LOAD MODULES
  grunt.loadNpmTasks "grunt-contrib-coffee"
  grunt.loadNpmTasks "grunt-contrib-concat"
  grunt.loadNpmTasks "grunt-contrib-uglify"
  grunt.loadNpmTasks "grunt-contrib-less"
  grunt.loadNpmTasks "grunt-angular-templates"

  # SETTINGS
  grunt.initConfig
    pkg: grunt.file.readJSON("package.json")

    concat:
      app:
        options:
          separator: ';'
          stripBanners: true
          banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
            '<%= grunt.template.today("yyyy-mm-dd") %> */',
        files:
          'public/assets/js/app.js': [
            "app/javascript/vendor/jquery.js"
            "app/javascript/vendor/angular.min.js"
            "app/javascript/vendor/*.js"

            "public/assets/js/app.compiled.js"
            "app/javascript/app.ngtemplates.js"
          ]

    uglify:
      options:
        sourceMap: true,
        sourceMapName: 'public/assets/js/app.min.js.map'
        compress: true
        mangle: false
      assets:
        files:
          'public/assets/js/app.min.js': ['public/assets/js/app.js']
          'public/assets/js/admin.min.js': ['public/assets/js/admin.js']

    coffee:
      app:
        options:
          bare: true
          sourceMap: true
        files:
          "public/assets/js/app.compiled.js": [
            "app/javascript/scraper.coffee"
            "app/javascript/components/*.coffee"
            "app/javascript/components/**/*.coffee"
          ]
          "public/assets/js/admin.js": [
            "app/javascript/admin/*.coffee"
          ]

    less:
      development:
        files:
          "public/assets/css/app.css": "app/stylesheet/app.less"

    ngtemplates:
      Scraper:
        cwd: "app/javascript"
        src: "**/*.html"
        dest: "app/javascript/app.ngtemplates.js"
        options:
          prefix: '/templates/'
          htmlmin:
            collapseWhitespace: true
            collapseBooleanAttributes: true


  # TASKS
  grunt.registerTask "compile:assets", ["coffee:app", "less:development", "ngtemplates:Scraper", "concat:app", "uglify"]