express = require "express"
config  = require "./../classes/config"

module.exports = do ->
  app = do express.Router

  routeConfig =
    environment : process.env.NODE_ENV
    apiuri      : "#{config.host}:#{config.port}"
    appfile     : config.appfile
    staticHost  : config.static_uri

  app.use (req, res)->
    res.render "notfound", routeConfig

  app