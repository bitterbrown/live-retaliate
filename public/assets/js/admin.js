
/*
  ADMIN CONTROLLER
 */
Scraper.controller("MainAdmin", [
  "$scope", "$rootScope", "$controller", function($scope, $rootScope, $controller) {
    angular.extend(this, $controller("Main", {
      $scope: $scope,
      $rootScope: $rootScope
    }));
    return $(window).bind("keydown", (function(_this) {
      return function(event) {
        if (event.metaKey === false && event.ctrlKey === false && event.shiftKey === false && !$(":focus").is(".filter-input")) {
          if (event.which === 67) {
            if (angular.element(".event-item.active").size() > 0) {
              $rootScope.$broadcast("event:mark", "crap", angular.element(".event-item.active").scope());
            }
          }
          if (event.which === 65) {
            if (angular.element(".event-item.active").size() > 0) {
              return $rootScope.$broadcast("event:mark", "awesome", angular.element(".event-item.active").scope());
            }
          }
        }
      };
    })(this));
  }
]);

//# sourceMappingURL=admin.js.map
