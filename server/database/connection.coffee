mongoskin   = require "mongoskin"
ObjectID    = mongoskin.ObjectID
config      = require "./../classes/config"

db = mongoskin.db "mongodb://127.0.0.1:27017/#{config.database}", safe: true
db.ObjectID = ObjectID

db.bind "events"
db.events.ensureIndex {id: 1}, {unique: true}, -> true

db.bind "live_events"
db.live_events.ensureIndex {id: 1}, {unique: true}, -> true

db.bind "bands"
db.bands.ensureIndex {name: 1}, {unique: true}, -> true

db.bind "tracked"
db.tracked.ensureIndex {name: 1}, {unique: true}, -> true



module.exports = db