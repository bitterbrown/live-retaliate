Rest = require "node-rest-client"

module.exports = do ->

  class SpotifyApi
    uri: "https://api.spotify.com/v1"

    getRelatedArtists: (id, next)->
      client = new Rest.Client
      client.get "#{@uri}/artists/#{id}/related-artists", (data)=>
        try
          next JSON.parse(data).artists if next?

    getArtistsByName: (name, next)->
      client = new Rest.Client
      client.get "#{@uri}/search", parameters: {q: name, type: "artist"}, (data)=>
        data = JSON.parse data
        if data?.error?
          next []
        else
          next data?.artists?.items if next?

    getTracks: (id, next)->
      client = new Rest.Client
      client.get "#{@uri}/artists/#{id}/top-tracks?country=GB", (data)->
        response = JSON.parse data

        next if response.error? then [] else response.tracks

  new SpotifyApi