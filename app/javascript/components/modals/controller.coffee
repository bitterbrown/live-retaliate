Scraper.controller "ModalController", ["$scope", "EventsService", "$rootScope", ($scope, EventsService, $rootScope)->

  @component = $(".modal-component")
  @resizeInt = false

  angular.extend $scope,

    show: false

    close: (event)->
      $scope.show = false if $(event.target).find(".modal-component").size() > 0

    markEvent: ->
      if @event?
        @event = new EventsService @event
        @event[@prop] = not @event[@prop]
        @event.$update =>
          $rootScope.$broadcast "event:update", @event
          @show = false

  # EVENTS

  $scope.$on "show:modal", (event, conf)=>
    angular.extend $scope, conf

    $scope.show = true
    $scope.$$phase || $scope.$apply "show"
    @component.height @component.find(".modal-content").outerHeight()
    @component.width @component.find(".modal-content").outerWidth()

  $scope.$on "hide:modal", =>
    $scope.show = false
    $scope.$$phase || $scope.$apply "show"

  $scope.$on "keypress", (event, keyevent)->
    if keyevent.which is 27
      $scope.show = false

      $scope.$$phase || $scope.$apply "show"

]