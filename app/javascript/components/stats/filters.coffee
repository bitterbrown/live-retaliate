Scraper.filter "noimage", ->
  (events, count)->
    list = (event for event in events when event.pictures.length is 0)

    if count is yes then list.length else list

#-----------------------------------------------------------------------------------------------------------------------

Scraper.filter "crap", ->
  (events, count)->
    list = (event for event in events when event.crap is true)

    if count is yes then list.length else list

#-----------------------------------------------------------------------------------------------------------------------

Scraper.filter "awesome", ->
  (events, count)->
    list = (event for event in events when event.awesome is true)

    if count is yes then list.length else list

#-----------------------------------------------------------------------------------------------------------------------

Scraper.filter "percent", ->
  (number, list)->
    parseInt((number/list.length)*100, 10) + "%"