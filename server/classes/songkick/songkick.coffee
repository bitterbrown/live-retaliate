Artist  = require "./songkick/artist"
User    = require "./songkick/user"
Venue   = require "./songkick/venue"
Utils   = require "./songkick/utils"
Sync    = require "sync"

class Songkick

  constructor: (api_key, username, default_format="json") ->
    @api_key = api_key

    @artist = new Artist(api_key, default_format)
    @user   = new User(api_key, username, default_format)
    @venue  = new Venue(api_key, default_format)

  getTrackedArtists: (page, next)->
    url = "/users/casapiton47/artists/tracked.json"
    params =
      apikey: @api_key
      per_page: 50
      page: page
      skResultKey: "artist"

    Utils.getClean url, params, next

  getRelatedArtists: (id, next)->
    url = "/artists/#{id}/similar_artists.json"
    params =
      apikey: @api_key
      per_page: 30
      page: 1
      skResultKey: "artist"

    Utils.getClean url, params, next

  getUpcomingEvents: (page, next)->
    url = "/metro_areas/24426/calendar.json"
    params =
      apikey: @api_key
      per_page: 50
      page: page
      skResultKey: "event"

    Utils.getClean url, params, next

  getVenueDetails: (id, next)->
    url = "/venues/#{id}.json"
    params =
      apikey: @api_key
      per_page: 1
      page: 1
      skResultKey: "venue"

    Utils.getClean url, params, next


sk = new Songkick "iZUfV3niJwBPNzyx", "casapiton47"

module.exports = sk
