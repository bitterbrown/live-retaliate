Scraper.factory "BandsService", ["$resource", "CONFIG", ($resource, CONFIG)->

  $resource "#{CONFIG.API_URI}/api/bands/:name", name: "@name",

    update:
      method: "PUT"

]


#-----------------------------------------------------------------------------------------------------------------------


Scraper.factory "EventsService", ['$resource', "CONFIG", ($resource, CONFIG)->

  $resource "#{CONFIG.API_URI}/api/events/:id", id: "@id",

    update:
      method: "PUT"

    getNextEvents:
      url: "#{CONFIG.API_URI}/api/events/bydate"
      isArray: true
      params:
        date: moment(hours: 0, minutes: 0)._d

    getTracks:
      url: "#{CONFIG.API_URI}/api/events/:id/tracks"
      method: "GET"
      isArray: true

    getRelatedArtists:
      url: "#{CONFIG.API_URI}/api/events/:id/related-artists"
      method: "GET"

    search:
      url: "#{CONFIG.API_URI}/api/events/search/:query"
      method: "GET"
      isArray: true
      params:
        query: "@query"

    searchCount:
      url: "#{CONFIG.API_URI}/api/events/search/:query"
      method: "GET"
      isArray: false
      params:
        query: "@query"

]

