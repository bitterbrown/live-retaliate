Models      = require "./../models/models"
db          = require "./../database/connection"
Sync        = require "sync"


liveEventExists = (id, next)->
  db.live_events.findOne id: id, (err, event)->
    if err is null
      next null, event isnt null
    else
      next null, err

# Find all events
db.events.find().toArray (err, events)->
  console.log "#{events.length} events found"
  # Check if event is present as live event
  Sync =>
    for __event in events
      exists = liveEventExists.sync null, __event.id

      if not exists
        console.log "===>INSERT START", __event.id, __event.displayName
        liveEvent = new Models.Live __event
        liveEvent.insert.sync liveEvent
        console.log "<===INSERT END", __event.id
      else
        console.log "skip ", __event.id

  , (err)->
    console.log "Sync exception - Possibly end of array", err