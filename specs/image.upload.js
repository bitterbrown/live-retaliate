require("coffee-script/register");

var ImageDownloader   = require(__dirname + "/../server/classes/imageDownloader");
var db = require("./../server/database/connection.coffee");
var Sync = require("sync");


db.bands.find().toArray(function (err, bands) {
  if(err == null) {
    Sync(function () {
      for (var i=0; i<bands.length; i++) {
        var band = bands[i];

        if(band.imageBest !== false && typeof(band.imageBest.url) == "string") {
          console.log ("Ok, upload this image", band.imageBest.url);

          var Downloader = new ImageDownloader();
          var status = Downloader.processThumbnail.sync(Downloader, {
            src: band.imageBest.url,
            dest: __dirname + "/../temp/"
          });

          console.log ("STATUS:::", status);
        }

      }
    });

  }
});

