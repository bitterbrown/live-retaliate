Songkick    = require "./../classes/songkick/songkick"
EventModel  = require "./../models/event"
db          = require "./../database/connection"
Sync        = require "sync"
config      = require "./../classes/config"


scrapeEvents = ->
  console.log "\n===> Get Events"

  for page in [1..config.pages_to_fetch]
    Songkick.getUpcomingEvents page, (events)->
      Sync =>
        for eventObj in events when eventObj.type
          event = new EventModel eventObj
          event.insert.sync event

do scrapeEvents


#-----------------------------------------------------------------------------------------------------------------------


saveTrackedArtists = (page, next)->
  Songkick.getTrackedArtists page, (artists)->
    if artists?.length > 0
      for artist in artists
        db.tracked.insert {id: artist.id, name: artist.displayName}, (err)->
          if err isnt null then console.error err.err

      saveTrackedArtists (page + 1), next
    else
      do next

scrapeTrackedArtists = ->
  Sync =>
    db.tracked.remove {}, ->
      console.log "TRACKED::START!"

      saveTrackedArtists 1, ->
        console.log "TRACKED::UPDATED!"

do scrapeTrackedArtists



setInterval ->
  do scrapeTrackedArtists
  do scrapeEvents
, 1000 * 60 * 60 * 24 # 1 day
