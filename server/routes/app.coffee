exec    = require("child_process").exec
express = require "express"
config  = require "./../classes/config"
Sync    = require "sync"
md5     = require "MD5"

isAdmin = (cookies)->
  cookies?.lr_admin? and cookies.lr_admin in ["3e3353e69f052fd3a50d3e2169ef4543", "9b62ef4578ef6ea88fc7e1e470191c1a"]

module.exports = do ->
  app = do express.Router

  routeConfig =
    environment : process.env.NODE_ENV
    apiuri      : "#{config.host}:#{config.port}"
    appfile     : config.appfile
    staticHost  : config.static_uri

  app.get '/', (req, res)->
    Sync =>
      if process.env.NODE_ENV is "development"
        exec.sync null, "grunt compile:assets"

      routeConfig.admin = isAdmin req.cookies
      res.render "index", routeConfig

  app.get '/stats', (req, res)->
    if isAdmin(req.cookies) is yes
      Sync =>
        exec.sync null, "grunt compile:javascript" if process.env.NODE_ENV is "development"
        res.render "index", routeConfig
    else
      res.redirect "/forbidden"

  app.get "/forbidden", (req, res)->
    res.render "forbidden", routeConfig

  app.get "/admin", (req, res)->
    if req.query?.email?
      res.cookie "lr_admin", md5("live-retaliate-admin__#{req.query.email}")

    res.redirect "/"

  app