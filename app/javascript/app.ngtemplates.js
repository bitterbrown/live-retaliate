angular.module('Scraper').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('/templates/components/event/view.html',
    "<loader ng-show=\"activeEvent == false\"></loader><div ng-show=\"activeEvent != false\" event-id=\"{{activeEvent.id}}\" class=\"is-{{activeEvent.type | lowercase}}\"><header ng-mouseover=\"hover = true\" ng-mouseleave=\"hover=false\" ng-class=\"{'hover': hover, 'no-image': activeEvent.pictureDetails == '' || activeEvent.pictureDetails == undefined}\"><div class=\"header-image\" style=\"background-image: url({{activeEvent.pictureDetails}})\"></div><img class=\"placeholder\" src=\"{{activeEvent.pictureDetails}}\" alt=\"\"><h5><small ng-if=\"activeEvent.end.date\">Start</small> {{activeEvent.start | fullEventDate}}</h5><h5 ng-if=\"activeEvent.end.date\"><small ng-if=\"activeEvent.end.date\">End</small> {{activeEvent.end | fullEventDate}}</h5></header><section class=\"bands\"><div class=\"bands-list\"><h5 ng-repeat=\"band in activeEvent.performance\"><a href=\"{{band.href}}\" target=\"_blank\">{{band.name}}</a></h5></div></section><section class=\"venue\"><div class=\"content\"><span ng-if=\"activeEvent.ageRestriction\" class=\"age-restriction\">{{activeEvent.ageRestriction}}</span><h3><a ng-href=\"{{activeEvent.venue.uri}}\" target=\"_blank\">{{activeEvent.venue.displayName}}</a></h3><h5 ng-if=\"activeEvent.venue.address != ''\">{{activeEvent.venue.address}}</h5><small ng-if=\"activeEvent.venue.phone != null && activeEvent.venue.phone != ''\">Tel: {{activeEvent.venue.phone}}</small> <a class=\"external-link\" href=\"{{activeEvent.uri}}\" target=\"_blank\">View event on SongKick</a></div><a class=\"map\" href=\"http://maps.google.com/maps?ll={{activeEvent.venue.lat}},{{activeEvent.venue.lng}}&amp;q={{activeEvent.venue.lat}},{{activeEvent.venue.lng}}&amp;z=15\" target=\"_blank\" style=\"background-image: url()\"><img width=\"100%\" src=\"http://maps.googleapis.com/maps/api/staticmap?center={{activeEvent.venue.lat}},{{activeEvent.venue.lng}}&amp;zoom=14&amp;size=300x200&amp;sensor=false&amp;markers=color%3A0xf80046%7C{{activeEvent.venue.lat}}%2C{{activeEvent.venue.lng}}&amp;\" alt=\"\"></a></section><section class=\"infos\" ng-class=\"{'has-tracks': activeEvent.tracks.length > 0}\"><div class=\"artists-list\" ng-if=\"relatedArtists\"><h5>Related artists</h5><div ng-if=\"related.length > 0\" ng-repeat=\"(band, related) in relatedArtists\" class=\"related-artists-list\"><h6 ng-if=\"activeEvent.performance.length > 1\">{{band | stripQuotes}}</h6><small ng-repeat=\"rel in related\">{{rel}}<span ng-if=\"!$last\">,&nbsp;</span></small></div></div><div class=\"player\" ng-show=\"activeEvent.tracks.length > 0\"><h5 ng-if=\"activeEvent.tracks.length > 0\">Listen</h5><iframe name=\"player\" src=\"\" width=\"100%\" height=\"330\" frameborder=\"0\" allowtransparency=\"true\"></iframe></div></section></div>"
  );


  $templateCache.put('/templates/components/events/view.html',
    "<loader ng-include=\"'/templates/components/main/loader.html'\" ng-show=\"events.length == 0 && searchQuery == ''\"></loader><p class=\"info\" ng-show=\"searchQuery != '' && events.length == 0\">No events match for search criteria!</p><div class=\"date-group-sep\" ng-if=\"$index > 0 && filteredEvents[$index-1].start.date != event.start.date\" ng-repeat-start=\"event in (filteredEvents = (events | filter:showCrapEvents)) \"></div><header event-index=\"{{$index}}\" ng-if=\"$index == 0 || filteredEvents[$index-1].start.date != event.start.date\" class=\"date-group\" ng-class=\"{'first': $first}\"><div class=\"date-group-content\"><h3 ng-bind-html=\"::event.start.date | date:'EEEE, d MMMM' | eventListDate\"></h3></div></header><div class=\"event-item\" ng-show=\"event.crap == false || showCrap == true\" id=\"{{::event.id}}\" ng-class=\"{'crap': event.crap == true, 'awesome': event.awesome == true, 'awesome-first': event.awesome == true && filteredEvents[$index-1].awesome != true}\" ng-repeat-end><div ng-click=\"openEvent(this, $event)\" class=\"event-item-container\" ng-class=\"{'cancelled': event.status == 'cancelled'}\" pictures-rodeo><div class=\"event-pictures-container\"><div ng-if=\"event.pictures.length > 0\" ng-repeat=\"picture in event.pictures\" class=\"event-picture\" style=\"background-image: url({{::staticHost}}/bands/{{::picture}})\"></div></div><div class=\"event-details\"><h3 class=\"media-heading\"><span ng-repeat=\"band in event.performance | limitTo: 10\"><bandname ng-class=\"{'is-awesome': band.awesome, 'is-crap': band.crap}\">{{::band.name}}</bandname><em ng-if=\"!$last\">,</em></span> <span ng-if=\"event.performance.length > 10\"><a class=\"and-more\">and more...</a></span> <span ng-if=\"event.performance.length == 0 && event.type == 'Festival'\">Line-up unknown</span></h3><h5>{{::event.venue.displayName}}</h5><h6 ng-if=\"event.type == 'Festival'\">Festival</h6></div></div></div><div class=\"load-more\" ng-hide=\"noMoreEvents\"><button ng-click=\"loadMoreEvents()\">Load more</button></div>"
  );


  $templateCache.put('/templates/components/footer/view.html',
    "<footer><p>2014 <a href=\"http://www.bitterbrown.com\" target=\"_blank\">Bitterbrown Ltd</a></p><a class=\"by-songkick\" href=\"http://www.songkick.com\" target=\"_blank\"><span>Concerts by Songkick</span></a> <img src=\"https://bitterbrown.dploy.io/badge/02267417958972/15325.png\" alt=\"Deployment status from dploy.io\"></footer>"
  );


  $templateCache.put('/templates/components/header/view.html',
    "<header ng-controller=\"HeaderController\"><h1 ng-click=\"$emit('event:hide')\" title=\"Live Retaliate\">Live retaliate</h1><input class=\"filter-input\" type=\"text\" ng-model=\"query\" ng-focus=\"startSearch()\" ng-blur=\"stopSearch()\" value=\"\" placeholder=\"Search\"><div class=\"search-actions\"><span class=\"results-count\">{{eventResults.length}}</span> <a class=\"search-reset\" ng-click=\"$broadcastAll('app:search:clear')\"></a></div></header>"
  );


  $templateCache.put('/templates/components/main/loader.html',
    "<div class=\"center\"><div class=\"bouncywrap\"><div class=\"dotcon dc1\"><div class=\"dot\"></div></div><div class=\"dotcon dc2\"><div class=\"dot\"></div></div><div class=\"dotcon dc3\"><div class=\"dot\"></div></div></div></div>"
  );


  $templateCache.put('/templates/components/main/view.html',
    "<section id=\"events-list-container\" ng-controller=\"EventsController\"><section id=\"events-filters\"><button ng-click=\"toggleCrapEvents()\">{{(showCrap == true) ? \"Hide\" : \"Show\"}} crap</button></section><section id=\"events-list\" infinite-load ng-include=\"'/templates/components/events/view.html'\"></section></section><section id=\"event-details\" class=\"palette-concrete\" ng-controller=\"EventController\" ng-class=\"{'has-player': hasPlayer}\" ng-include=\"'/templates/components/event/view.html'\"></section>"
  );


  $templateCache.put('/templates/components/modals/view.html',
    "<div class=\"modal-container\" ng-class=\"{'hide': show == false}\" ng-click=\"close($event)\"><div class=\"modal-component\"><div class=\"modal-content\"><h2 ng-if=\"title\">{{title}}</h2><p ng-if=\"description\">{{description}}</p><button band-button ng-repeat=\"button in buttons\" property=\"{{prop}}\"></button></div><footer><button class=\"action-button\" ng-click=\"markEvent()\">Mark EVENT as {{prop}}</button></footer></div></div>"
  );


  $templateCache.put('/templates/components/stats/view.html',
    "<div style=\"padding: 0 20px\"><h1>Stats</h1><h3>Summary</h3><table><tr><td>Total events</td><td><strong>{{events.length}}</strong></td></tr><tr><td>Total crap events</td><td><strong>{{events | crap:true}} ({{events | crap:true | percent:events}})</strong></td></tr><tr><td>Total awesome events</td><td><strong>{{events | awesome:true}} ({{events | awesome:true | percent:events}})</strong></td></tr><tr><td>Total bands</td><td><strong>{{bands.length}}</strong></td></tr></table><hr><h3>Events with data missing</h3></div>"
  );

}]);
