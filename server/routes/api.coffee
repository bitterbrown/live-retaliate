express     = require "express"
bodyParser  = require "body-parser"
db          = require "../database/connection"
Models      = require "../models/models"
Sync        = require "sync"


module.exports = do ->
  api = do express.Router

  api.use do bodyParser.json
  api.use bodyParser.urlencoded({
    extended: true
  })

  api.use (req, res, next)->
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Origin', req.headers.origin)
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version');

    do next


  ###
  EVENTS ---------------------------------------------------------------------------------------------------------------
  ###

  api.get "/events/bydate", (req, res)->
    today = new Date req.query.date

    db.live_events.find("start.date": today).sort("start.date": 1, awesome: -1).toArray (error, events)->
      return error if error?

      res.json events


  api.get "/events/search/:query", (req, res)->
    today = new Date()
    query =
      "start.date": {$gte: new Date()}
      "$or": [
        {"displayName": new RegExp(req.params.query, "gi")}
        {"performance.name": new RegExp(req.params.query, "gi")},
        {"venue.displayName": new RegExp(req.params.query, "gi")}
      ]

    if req.query.count?
      db.live_events.count query, (error, count)->
        res.json count: count
    else
      db.live_events.find(query).sort("start.date": 1, awesome: -1).toArray (error, events)->
        return error if error?

        res.json events


  api.get "/events/:id/related-artists", (req, res)->
    db.live_events.findOne id: parseInt(req.params.id, 10), (err, event)->
      if event is null or err isnt null
        do res.status(if event is null then 404 else 500).end

      else
        related = {}
        db.bands.find({name: {$in: (band.name for band in event.performance)}}).toArray (err, bands)->
          for band in bands
            related['"'+band.name+'"'] = band.related

          res.json related


  api.get "/events/:id/tracks", (req, res)->
    db.live_events.findOne id: parseInt(req.params.id, 10), (err, event)->
      if event is null or err isnt null
        do res.status(if event is null then 404 else 500).end

      else
        tracks = []
        db.bands.find({name: {$in: (band.name for band in event.performance)}}).toArray (err, bands)->
          for band in bands
            tracks.push track for track in band.tracks

          res.json tracks


  api.get "/events", (req, res)->
    db.collection("events").find().sort("start.date": 1, awesome: -1).toArray (error, results)->
      return error if error?

      res.json results


  api.get "/events/:id", (req, res)->
    db.live_events.findOne id: parseInt(req.params.id, 10), (err, event)->
      if event is null or err isnt null
        do res.status(if event is null then 404 else 500).end

      else
        res.json event


  api.put "/events/:id", (req, res)->
    event = new Models.Live id: parseInt(req.params.id, 10)
    for name,value of req.body when name isnt "_id"
      event.set name, value

    event.update ->
      res.json event.attributes


  ###
  bands ----------------------------------------------------------------------------------------------------------------
  ###

  api.get "/bands", (req, res)->
    db.collection("bands").find().toArray (error, results)->
      return error if error?

      res.json results


  api.get "/bands/:name", (req, res)->
    Sync =>
      band = new Models.Band name: req.params.name
      band.fetch.sync band
      res.json do band.getCoreAttributes


  api.put "/bands/:name", (req, res)->
    band = new Models.Band name: req.params.name
    band.fetch ->
      for name,value of req.body
        band.set name, value

      band.save ->
        res.json do band.getCoreAttributes