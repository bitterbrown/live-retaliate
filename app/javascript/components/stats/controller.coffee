Scraper.controller "StatsController", ["$scope", "EventsService", "BandsService", "$http", ($scope, EventsService, BandsService, $http)->

  $scope.events = EventsService.get()
  $scope.bands = BandsService.get()

  $scope.showMissingImageList = false
  $scope.showMissingTrackList = false

  $scope.fetchImage = (scope, $event)->
    $http.get("/api/events/#{scope.event.id}/update").then (resp)->
      $($event.currentTarget).html if resp.status is 200 then "Request sent" else "Error!"


#  today = new Date()
#  today.setHours 0
#  today.setMinutes 0
#  today.setSeconds 0

#  $scope.$watch "events", ->
#    $.extend $scope,
#      futureEvents: (event for event in events when event.)
#    , true


]