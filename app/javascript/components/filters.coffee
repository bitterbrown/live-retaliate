Scraper.filter "fullEventDate", ->
  (input)->
    if input?
      format = "dddd - MMMM Do - YYYY#{if input.datetime? then ", h:mm a" else ""}"
      date = if input.datetime? then input.datetime else input.date
      moment(date).format format
    else
      false

#-----------------------------------------------------------------------------------------------------------------------

Scraper.filter "removeVenue", ->
  (input)->
    return input if input.indexOf("at") is -1
    input.substring 0, input.lastIndexOf("at")

#-----------------------------------------------------------------------------------------------------------------------

Scraper.filter "stripQuotes", ->
  (input)->
    return input.split('"').join("")

#-----------------------------------------------------------------------------------------------------------------------

Scraper.filter "eventListDate", ($sce)->
  (input)->
    unless not input?
      input = input.replace /(\,+)/g, ""
      input = input.replace /([a-zA-Z0-9\-\_]+)\s/g, "<span>$1</span>"
      $sce.trustAsHtml input
