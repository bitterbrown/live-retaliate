Scraper.controller "Main", ["$scope", "$rootScope", ($scope, $rootScope)->

  $scope.$broadcastAll = ->
    $rootScope.$broadcast.apply $rootScope, arguments

  $scope.$on "event:show", ->
    $("html").addClass "has-event"
    false

  $scope.$on "event:hide", ->
    $("html").removeClass "has-event"
    false

  $scope.$on "app:search:start", ->
    $("html")
      .removeClass "has-search-events"
      .addClass "is-searching"

  $scope.$on "app:search:stop", ->
    $("html").removeClass "is-searching"
    false

  $scope.$on "app:search:show", ->
    $("html").addClass "has-search-events"

  $scope.$on "app:search:clear", ->
    $("html").removeClass "has-search-events"

  $(window).on "keydown", (event)=>
#    console.log "event.which", event.which

    if event.metaKey is false and event.ctrlKey is false and event.shiftKey is false and not $(":focus").is(".filter-input")
      if event.which in [37, 38, 39, 40]  #arrow keys
        do event.stopPropagation
        do event.preventDefault

        $rootScope.$broadcast "goto:event", event.which - 39 if event.which in [38, 40]
        $rootScope.$broadcast "goto:date", event.which - 38 if event.which in [37, 39]

      if event.which is 13  #enter
        $rootScope.$broadcast "event:show", angular.element(".event-item.active").scope()

      $rootScope.$broadcast "keypress", event

    if $("html").is ".is-searching"
      $rootScope.$broadcast "keypress", event

]