Scraper = angular.module "Scraper", ["ngResource", "ngRoute"]

Scraper.config ['$routeProvider', ($routeProvider)->

  $routeProvider.when "/",
    templateUrl: "/templates/components/main/view.html",

  $routeProvider.when "/stats",
    controller: "StatsController"
    templateUrl: "/templates/components/stats/view.html",

]

Scraper.constant "CONFIG",
  ENV           : LiveEnvironment
  API_URI       : LiveApiUri
  LiveStaticHost: LiveStaticHost

Scraper.run  ->
  Scraper.config =
    isPhone: $("body").width() < 600
