require("coffee-script/register");

var Models = require("./../server/models/models.coffee"),
    Songkick = require("./../server/classes/songkick/songkick")

Songkick.getUpcomingEvents(1, function (events) {
  var event = new Models.Event(events[0]);
  event.delete(function () {
    event.insert();
  });
});
//
//var band = new BandModel({name: "Littlebrown"});  // Not present, scrape it from songkick
//band.fetch();
