Utils = require './utils'

class User

  constructor: (@api_key, @username) ->
    @calendar = @_calendar

  _calendar: (options, callback) ->
    url = "/users/#{@username}/calendar.json"
    params =
      apikey: @api_key
      per_page: 50
      page: options.page? | 1
      reason: "tracked_artist"
      skResultKey: "calendarEntry"

    if typeof options is "function"
      callback = options
      options = {}

    Utils.getClean url, Utils.merge(params,options), callback

module.exports = User
