LiveModel       = require "./model"
Sync            = require "sync"

module.exports = do ->

  class EventModel extends LiveModel
    collectionName: "events"


    afterInsert: ->
      LiveEvent = require "./live-event"

      Sync =>
        live = new LiveEvent @attributes
        live.create.sync live