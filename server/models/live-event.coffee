Sync            = require "sync"
LiveModel       = require "./model"
BandModel       = require "./band"
EventModel      = require "./event"
SongkickApi     = require "./../classes/songkick/songkick"


module.exports = do ->

  class LiveEvent extends LiveModel
    collectionName: "live_events"


    isAwesome: (next)->
      return @returns true, next if @get("awesome") is yes

      Sync =>
        for _band in @get("performance")
          band = new BandModel name: _band.name
          band.fetch.sync band

          @set "awesome", yes if band.get("awesome") is yes

          return next null

    venueDetails: (next)->
      SongkickApi.getVenueDetails @get("venue")?.id, (venue)=>
        if venue?
          evVenue = @get "venue"
          evVenue.address = "#{venue.street} #{venue.zip}"
          evVenue.phone = venue.phone
          @set "venue", evVenue
          @dirty = yes

        next null

    reflect: (next)->
      # Reflecting CRAP
      if @get("awesome") is no
        @set "crap", @get("performance").length > 0 and (band for band in @get("performance") when band.crap is yes).length is @get("performance").length
      else
        @set "crap", false

      # Reflecting AWESOME
      @set "awesome", yes if (band for band in @get("performance") when band.awesome is yes).length is @get("performance").length

      next null

    create: (next)->
      EventModel = require "./event"

      return console.error "Error creating Live event - missing key (#{@key})" if not @get(@key)?

      Sync =>
        event = new EventModel do @getQuery
        event.fetch.sync event

        @attributes = @parse event.attributes

        @insert next

    afterInsert: ->
      Sync =>
        @isAwesome.sync @
        @venueDetails.sync @

        do @update if @dirty is yes

    save: (next)->
      Sync =>
        @reflect.sync @
        @update.sync @

        next null, @attributes

    parse: (data)->
      BandModel = require "./band"

      data.pictureDetails = ""
      bandArray = []

      if data.performance?
        for _band in data.performance
          band = new BandModel _band
          band.fetch.sync band
          bandArray.push band.getCoreAttributes()
          data.pictureDetails = band.get("imageBest").url if band.get("imageBest")?.url? and data.pictureDetails is ""

        data.performance = bandArray

      if data.start?.date? then data.start.date = new Date(data.start.date)
      if data.end?.date? then data.end.date = new Date(data.end.date)

      id              : data.id
      type            : data.type || ""
      displayName     : data.displayName || ""
      status          : data.status || "ok"
      start           : data.start || null
      end             : data.end || null
      performance     : data.performance || []
      venue           :
        displayName   : data.venue?.displayName
        lat           : data.venue?.lat
        lng           : data.venue?.lng
        uri           : data.venue?.uri
        id            : data.venue?.id
        address       : data.address || ""
        phone         : data.phone || ""
      uri             : data.uri || "#"
      ageRestriction  : data.ageRestriction || null

      awesome         : data.awesome || false
      crap            : data.crap || false
      pictures        : data.pictures || (band.thumbnail for band in bandArray when band.thumbnail isnt false)
      pictureDetails  : data.pictureDetails || ""