Scraper.directive "bandButton", ["$rootScope", ($rootScope)->
  restrict: "EA"
  link: (scope, $el, attrs)->
    scope.$on "band:update", (ev, band)->
      scope.button = band if scope.button.name is band.name

    scope.$watch "button", ->
      $el
      .attr "class", ""
      .addClass "button #{if scope.button.awesome then "is-awesome"} #{if scope.button.crap then "is-crap"}"
      .html scope.button.name
      .on "click", =>
        $rootScope.$broadcast "set:band", attrs.property, scope.button

]


#-----------------------------------------------------------------------------------------------------------------------


Scraper.directive "infiniteLoad", ->
  restrict: 'A'
  link: (scope, element)->

    element.on "scroll", =>
      unless scope.searchQuery isnt ""
        if not Scraper.config.isPhone or (Scraper.config.isPhone is yes and $("html").is(":not(.has-event)"))
          if element.children(":last").prev().offset().top - element.height() < 400
            do scope.loadMoreEvents

        else if Scraper.config.isPhone is yes and $("html").is(".has-event")
          if element.children(":last").prev().offset().left < $("body").width()
            do scope.loadMoreEvents


#-----------------------------------------------------------------------------------------------------------------------


Scraper.directive "modalContainer", ->
  restrict: 'E'
  controller: "ModalController"
  templateUrl: "/templates/components/modals/view.html"


#-----------------------------------------------------------------------------------------------------------------------


Scraper.directive "picturesRodeo", ($timeout)->
  restrict: 'A'
  link: (scope, element, attr)->
    intId = 0

    animate = ->
      return clearInterval intId if element.find(".event-picture").size() is 0

      if element.find(".event-picture").size() > 1
        intId = setInterval =>
          element.find(".event-picture:not(.done):last").addClass("done")
          if element.find(".event-picture:not(.done)").size() is 0
            element.find(".event-picture").removeClass "done"

        , Math.random(0,1)*1500 + 3500

    if attr.picturesRodeo isnt ""
      scope.$watch attr.picturesRodeo, ->
        clearInterval intId
        do animate

    else
      $timeout =>
        do animate
