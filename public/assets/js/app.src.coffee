Scraper = angular.module "Scraper", ["ngResource", "ngRoute"]

Scraper.config ['$routeProvider', ($routeProvider)->

  $routeProvider.when "/",
    templateUrl: "/assets/js/components/main/view.html",

  $routeProvider.when "/stats",
    controller: "StatsController"
    templateUrl: "/assets/js/components/stats/view.html",

]

Scraper.constant "ENV", LiveEnvironment
Scraper.constant "API_URI", LiveApiUri

Scraper.run  ->
  Scraper.config =
    isPhone: $("body").width() < 600

Scraper.directive "bandButton", ["$rootScope", ($rootScope)->
  restrict: "EA"
  link: (scope, $el, attrs)->
    scope.$on "band:update", (ev, band)->
      scope.button = band if scope.button.name is band.name

    scope.$watch "button", ->
      $el
      .attr "class", ""
      .addClass "button #{if scope.button.awesome then "is-awesome"} #{if scope.button.crap then "is-crap"}"
      .html scope.button.name
      .on "click", =>
        $rootScope.$broadcast "set:band", attrs.property, scope.button

]


#-----------------------------------------------------------------------------------------------------------------------


Scraper.directive "infiniteLoad", ->
  restrict: 'A'
  link: (scope, element)->

    element.on "scroll", =>
      unless scope.searchQuery isnt ""
        if not Scraper.config.isPhone or (Scraper.config.isPhone is yes and $("html").is(":not(.has-event)"))
          if element.children(":last").prev().offset().top - element.height() < 400
            do scope.loadMoreEvents

        else if Scraper.config.isPhone is yes and $("html").is(".has-event")
          if element.children(":last").prev().offset().left < $("body").width()
            do scope.loadMoreEvents


#-----------------------------------------------------------------------------------------------------------------------


Scraper.directive "modalContainer", ->
  restrict: 'E'
  controller: "ModalController"
  templateUrl: "/assets/js/components/modals/view.html"


#-----------------------------------------------------------------------------------------------------------------------


Scraper.directive "picturesRodeo", ($timeout)->
  restrict: 'A'
  link: (scope, element, attr)->
    intId = 0

    animate = ->
      return clearInterval intId if element.find(".event-picture").size() is 0

      if element.find(".event-picture").size() > 1
        intId = setInterval =>
          element.find(".event-picture:not(.done):last").addClass("done")
          if element.find(".event-picture:not(.done)").size() is 0
            element.find(".event-picture").removeClass "done"

        , Math.random(0,1)*1500 + 3500

    if attr.picturesRodeo isnt ""
      scope.$watch attr.picturesRodeo, ->
        clearInterval intId
        do animate

    else
      $timeout =>
        do animate

Scraper.filter "fullEventDate", ->
  (input)->
    if input?
      format = "dddd - MMMM Do - YYYY#{if input.datetime? then ", h:mm a" else ""}"
      date = if input.datetime? then input.datetime else input.date
      moment(date).format format
    else
      false

#-----------------------------------------------------------------------------------------------------------------------

Scraper.filter "removeVenue", ->
  (input)->
    return input if input.indexOf("at") is -1
    input.substring 0, input.lastIndexOf("at")

#-----------------------------------------------------------------------------------------------------------------------

Scraper.filter "stripQuotes", ->
  (input)->
    return input.split('"').join("")

#-----------------------------------------------------------------------------------------------------------------------

Scraper.filter "eventListDate", ($sce)->
  (input)->
    unless not input?
      input = input.replace /(\,+)/g, ""
      input = input.replace /([a-zA-Z0-9\-\_]+)\s/g, "<span>$1</span>"
      $sce.trustAsHtml input

Scraper.factory "BandsService", ["$resource", "API_URI", ($resource, API_URI)->

  $resource "#{API_URI}/api/bands/:name", name: "@name",

    update:
      method: "PUT"

]


#-----------------------------------------------------------------------------------------------------------------------


Scraper.factory "EventsService", ['$resource', "API_URI", ($resource, API_URI)->

  $resource "#{API_URI}/api/events/:id", id: "@id",

    update:
      method: "PUT"

    getNextEvents:
      url: "#{API_URI}/api/events/bydate"
      isArray: true
      params:
        date: moment(hours: 0, minutes: 0)._d

    getTracks:
      url: "#{API_URI}/api/events/:id/tracks"
      method: "GET"
      isArray: true

    getRelatedArtists:
      url: "#{API_URI}/api/events/:id/related-artists"
      method: "GET"

    search:
      url: "#{API_URI}/api/events/search/:query"
      method: "GET"
      isArray: true
      params:
        query: "@query"

    searchCount:
      url: "#{API_URI}/api/events/search/:query"
      method: "GET"
      isArray: false
      params:
        query: "@query"

]


Scraper.controller "EventController", ["$scope", "$rootScope", "$http", "EventsService", ($scope, $rootScope, $http, EventsService)->

  $scope.activeEvent = false

  getTracksUri = (tracks)->
    if tracks.length > 40
      tracks = (track for track,i in tracks when i%(parseInt(tracks.length/40)) is 0)

    return (track.replace("spotify:track:", "") for track in tracks)

  $scope.$on "event:show", (ev, scope)->
    angular.extend $scope,
      activeEvent     : scope.event
      relatedArtists  : false

    EventsService.getTracks id: scope.event.id, (tracks)->
      tracks = getTracksUri tracks
      $scope.activeEvent.tracks = tracks

      if tracks.length > 0
        window.player.location.href = "https://embed.spotify.com/?&uri=spotify:trackset:Listen:#{tracks.join(",")}&view=list"

    EventsService.getRelatedArtists id: scope.event.id, (related)->
      if (band for band of related when related[band].length > 0).length > 0
        $scope.relatedArtists = related

    $rootScope.$$phase || $scope.$apply "activeEvent"

]

Scraper.controller "EventsController", ["$scope", "$rootScope", "EventsService", "BandsService", "$timeout", "ENV", ($scope, $rootScope, EventsService, BandsService, $timeout, ENV) ->

  isLoading   = false
  dateLimit   = moment hours: 0, minutes: 0
  activeEvent = -1

  updateRelatedEvents = (band)->
    for event,i in $scope.events
      if event.performance? and (_band for _band in event.performance when _band.name is band.name).length > 0
        $scope.events[i] = EventsService.get id: event.id

  highlightRequiredDate = (method)->
    $active = $(".event-item:eq(#{activeEvent})")

    $header = $active["#{method}All"]("header[event-index!=#{$active.attr("event-index")}]:first")

    if $header.size() is 0 # Don't have any header to show, let's add more chunks
      @loadMoreEvents =>
        $timeout ->
          highlightRequiredDate method
        , 0

    else # Next date header found
      activeEvent = new Number $header.attr("event-index")
      $scope.$broadcastAll "goto:event", 0

  progressiveLoad = (index, events, next)->
    if index < events.length
      $timeout ->
        $scope.events.push events[index]
        index++
        progressiveLoad index, events, next
      , 50

    else
      do next if next?

  angular.extend $scope,
    events            : do EventsService.getNextEvents
    showCrap          : false
    isPhone           : Scraper.config.isPhone
    noMoreEvents      : false
    searchQuery       : ""

    toggleCrapEvents: ->
      @showCrap = not @showCrap

    openEvent: (scope)->
      if scope.event.status isnt "cancelled"
        $scope.$broadcastAll "goto:event", scope.$index, true
        $scope.$broadcastAll "event:show", scope

        console.log "Open event", scope.event if ENV is "development"

    loadMoreEvents: (callback)->
      if isLoading is no and @noMoreEvents isnt true
        isLoading = yes

        dateLimit.add 1, "day"

        EventsService.getNextEvents
          date: dateLimit._d
        , (events)=>
          @noMoreEvents = events.length is 0

          if events.length > 0
            @events.push event for event in events

            $rootScope.$$phase || @$apply "events"
            isLoading = no

            do callback if callback?

#            progressiveLoad 0, events, =>
#              isLoading = no

        , (error)->
          do callback if callback?

  $scope.showCrapEvents = (event)->
    if $scope.showCrap is true then return true else return event.crap is false



  #---------------------------------------------------------------------------------------------------------------------

  $scope.$on "app:search:show", (ev, events, searchQuery)->
    $timeout ->
      $scope.events = events
      $scope.searchQuery = searchQuery

  $scope.$on "app:search:clear", ->
    $timeout ->
      $scope.events = do EventsService.getNextEvents
      $scope.searchQuery = ""

  $scope.$on "goto:event",
    (ev, dir, force)->
      return if activeEvent is $scope.events.length - 1 and dir is 1

      $(".event-item.active").removeClass "active"

      activeEvent += dir
      activeEvent = dir if force is true
      activeEvent = if activeEvent < 0 then 0 else activeEvent
      activeElem = $(".event-item:eq(#{activeEvent})")
      activeElem.addClass "active"

      $timeout ->
        if (activeElem.offset().top + activeElem.height()) > $("#events-list").height() - 40
          $("#events-list").stop().animate
            scrollTop: $("#events-list").scrollTop() + activeElem.position().top - $("#events-list").height()/2
            scrollLeft: $("#events-list").scrollLeft() + activeElem.position().left - $("#events-list").width()/2
          , 300

        if activeElem.offset().top < 0
          $("#events-list").stop().animate
            scrollTop: $("#events-list").scrollTop() + activeElem.position().top
            scrollLeft: $("#events-list").scrollLeft() + activeElem.position().left
          , 300
      , if $("html").hasClass "has-event" then 0 else 500


  $scope.$on "event:mark",
    (_event, property, scope)->
      modalConf =
        title: "Mark as #{property}!",
        className: "#{property}-modal"
        buttons: []

      if scope.event.performance.length is 1
        $scope.$emit "set:band", property, scope.event.performance[0]
      else
        $scope.$broadcastAll "show:modal",
          title   : "Mark as #{property}!"
          prop    : property
          buttons : scope.event.performance
          event   : scope.event

  $scope.$on "set:band",
    (_event, property, _band)->
      band = new BandsService _band
      band[property] = not _band[property]
      band.$update ->
        $scope.$broadcastAll "band:update", band
        updateRelatedEvents band

  #        $("button:contains(#{band.name})").toggleClass "has-been-crapped", band.crap is yes
  #
  #        if band.crap is true
  ##          $("html").addClass "trash-garbage"
  #          $("button:contains(#{band.name})").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', ->
  ##            $("html").removeClass "trash-garbage"
  #            do $(this).remove
  #            $scope.$broadcastAll "hide:modal" if $(".modal-content button:not(.has-been-crapped)").size() is 0
  #          ).addClass "animated bounceOutDown"


  $scope.$on "goto:date",
    (ev, dir)=>
      highlightRequiredDate if dir > 0 then "next" else "prev"


  $scope.$on "event:update", (ev, liveEvent)->
    for event,i in $scope.events when event.id is liveEvent.id
      $scope.events[i] = liveEvent

]
Scraper.controller "HeaderController", ["$scope", "$rootScope", "EventsService", "$element", ($scope, $rootScope, EventsService, $element)->

  $scope.eventResults = []
  $scope.eventResultsCount = 0

  getResultsCount = ->
    if $scope.query? and $scope.query isnt ""
      EventsService.searchCount {query: $scope.query, count: true}, (result)->
        $scope.eventResultsCount = result.count

  getResults = (next)->
    if $scope.query? and $scope.query isnt ""
      EventsService.search query: $scope.query, (events)->
        $scope.eventResults = events
        do next

  $scope.startSearch = ->
    $scope.$emit "app:search:start"
    do getResultsCount

  $scope.stopSearch = ->
    $scope.eventResults = []
    $scope.$emit "app:search:stop"

  $scope.$watch "query", ->
    do getResultsCount

  $scope.$on "app:search:clear", ->
    $scope.query = ""

  $scope.$on "keypress", (ev, event)=>
    if event.which is 13 and $(":focus").is ".filter-input"
      if $scope.eventResultsCount > 0
        getResults ->
          $scope.$broadcastAll "app:search:show", $scope.eventResults, $scope.query
      else
        $scope.$broadcastAll "app:search:clear"

      $element.find(".filter-input").trigger "blur"

]
Scraper.controller "Main", ["$scope", "$rootScope", ($scope, $rootScope)->

  $scope.$broadcastAll = ->
    $rootScope.$broadcast.apply $rootScope, arguments

  $scope.$on "event:show", ->
    $("html").addClass "has-event"
    false

  $scope.$on "event:hide", ->
    $("html").removeClass "has-event"
    false

  $scope.$on "app:search:start", ->
    $("html")
      .removeClass "has-search-events"
      .addClass "is-searching"

  $scope.$on "app:search:stop", ->
    $("html").removeClass "is-searching"
    false

  $scope.$on "app:search:show", ->
    $("html").addClass "has-search-events"

  $scope.$on "app:search:clear", ->
    $("html").removeClass "has-search-events"

  $(window).on "keydown", (event)=>
#    console.log "event.which", event.which

    if event.metaKey is false and event.ctrlKey is false and event.shiftKey is false and not $(":focus").is(".filter-input")
      if event.which in [37, 38, 39, 40]  #arrow keys
        do event.stopPropagation
        do event.preventDefault

        $rootScope.$broadcast "goto:event", event.which - 39 if event.which in [38, 40]
        $rootScope.$broadcast "goto:date", event.which - 38 if event.which in [37, 39]

      if event.which is 13  #enter
        $rootScope.$broadcast "event:show", angular.element(".event-item.active").scope()

      $rootScope.$broadcast "keypress", event

    if $("html").is ".is-searching"
      $rootScope.$broadcast "keypress", event

]
Scraper.controller "ModalController", ["$scope", "EventsService", "$rootScope", ($scope, EventsService, $rootScope)->

  @component = $(".modal-component")
  @resizeInt = false

  angular.extend $scope,

    show: false

    close: (event)->
      $scope.show = false if $(event.target).find(".modal-component").size() > 0

    markEvent: ->
      if @event?
        @event = new EventsService @event
        @event[@prop] = not @event[@prop]
        @event.$update =>
          $rootScope.$broadcast "event:update", @event
          @show = false

  # EVENTS

  $scope.$on "show:modal", (event, conf)=>
    angular.extend $scope, conf

    $scope.show = true
    $scope.$$phase || $scope.$apply "show"
    @component.height @component.find(".modal-content").outerHeight()
    @component.width @component.find(".modal-content").outerWidth()

  $scope.$on "hide:modal", =>
    $scope.show = false
    $scope.$$phase || $scope.$apply "show"

  $scope.$on "keypress", (event, keyevent)->
    if keyevent.which is 27
      $scope.show = false

      $scope.$$phase || $scope.$apply "show"

]
Scraper.controller "StatsController", ["$scope", "EventsService", "BandsService", "$http", ($scope, EventsService, BandsService, $http)->

  $scope.events = EventsService.get()
  $scope.bands = BandsService.get()

  $scope.showMissingImageList = false
  $scope.showMissingTrackList = false

  $scope.fetchImage = (scope, $event)->
    $http.get("/api/events/#{scope.event.id}/update").then (resp)->
      $($event.currentTarget).html if resp.status is 200 then "Request sent" else "Error!"


#  today = new Date()
#  today.setHours 0
#  today.setMinutes 0
#  today.setSeconds 0

#  $scope.$watch "events", ->
#    $.extend $scope,
#      futureEvents: (event for event in events when event.)
#    , true


]
Scraper.filter "noimage", ->
  (events, count)->
    list = (event for event in events when event.pictures.length is 0)

    if count is yes then list.length else list

#-----------------------------------------------------------------------------------------------------------------------

Scraper.filter "crap", ->
  (events, count)->
    list = (event for event in events when event.crap is true)

    if count is yes then list.length else list

#-----------------------------------------------------------------------------------------------------------------------

Scraper.filter "awesome", ->
  (events, count)->
    list = (event for event in events when event.awesome is true)

    if count is yes then list.length else list

#-----------------------------------------------------------------------------------------------------------------------

Scraper.filter "percent", ->
  (number, list)->
    parseInt((number/list.length)*100, 10) + "%"