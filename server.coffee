require "coffee-script/register"
config            = require "./server/classes/config"
express           = require "express"
cookieParser      = require "cookie-parser"
compression       = require "compression"
ApiRoutes         = require "./server/routes/api"
AppRoutes         = require "./server/routes/app"
ErrorRoutes       = require "./server/routes/errors"

app = do express

port = config.port || 8888

app.set "views", __dirname + '/public/templates'
app.set "view engine", "jade"

app.use compression(threshold: 512)
app.use cookieParser()
app.use express.static(__dirname + '/public', magAge: "1d")

app.use "/",    AppRoutes
app.use "/api", ApiRoutes
app.use "*",    ErrorRoutes

app.listen port

console.log "Server started on port #{port}"
