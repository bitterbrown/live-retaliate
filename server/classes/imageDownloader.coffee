fs                = require "fs"
request           = require "request"
easyimage         = require "easyimage"
md5               = require "MD5"
AWS               = require "aws-sdk"
AWS.config.update require("./config").amazon
AWS.config.region = "eu-west-1"

class ImageDownloader

  name: false

  constructor: ->
    @s3 = new AWS.S3()

  processThumbnail: (options, next)->
    if options.src? and options.src isnt null and options.src isnt ""
      @name = md5 options.src
      @imageUri = "#{options.dest}#{md5(options.src)}"

      @download options.src, @imageUri, =>
        easyimage.thumbnail(
          src: @imageUri
          dst: @imageUri
          quality: 100
          width: 120
          height: 120
        ).then =>
          @upload next

    else
      console.error "Error processing thumbnail, must specify a SRC attribute, got:", options
      next null

  download: (fileUri, filename, next)->
    try
      request.head fileUri

      r = request(fileUri).pipe(fs.createWriteStream(filename))
      r.on "close", next

  upload: (next)->
    params =
      Bucket: "live-retaliate"
      Key: "bands/#{@name}"
      ACL: "public-read"

    fs.readFile @imageUri, (err, fileData)=>
      console.error "Error reading picture #{@name}" if err isnt null
      params.Body = fileData
      @s3.putObject params, (err, data)=>
        if err is null
          console.log "AWS:S3::UPLOAD_COMPLETE (#{@name})"
        else
          console.error "AWS:S3::UPLOAD_ERROR (#{@name})"

        do @unlink
        next null, err is null

  unlink: ->
    if @imageUri?
      try fs.unlinkSync @imageUri



module.exports = ImageDownloader