Models      = require "./../models/models"
db          = require "./../database/connection"
Sync        = require "sync"


# Get band properties and emit them across all live events
db.bands.find(found: true).toArray (err, bands)->
  for _band in bands
    band = new Models.Band name: _band.name
    do band.emit