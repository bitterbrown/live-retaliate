function perc(n, t) {
  if(n === 0)
    return "(0%)";
  else
    return "(" + (n/t*100).toFixed(2) + "%)";
}
var today = new Date();

var Events = {
  count       : db.events.count()
}

function findMissingEvents() {
  var notFound = [];
  db.events.find().forEach(function (event) {
    var found = db.live_events.count({id: event.id});
    if (found == 0)
      notFound.push(event.id);
  });
  return notFound;
}

var Live = {
  total       : db.live_events.count(),
  awesome     : db.live_events.count({awesome: true}),
  crap        : db.live_events.count({crap: true}),
  missing     : Events.count-db.live_events.count(),
  missingIds  : findMissingEvents(),
  festival    : db.live_events.count({type: "festival"}),
  past        : db.live_events.count({"start.date": {$lte: today}}),
  latestDate  : function () {
    var res = false
    db.live_events.find().sort({"start.date": -1}).limit(1).forEach(function (live) {
      res = live.start.date.toString();
    });
    return res;
  }(),
  firstDate  : function () {
    var res = false
    db.live_events.find().sort({"start.date": 1}).limit(1).forEach(function (live) {
      res = live.start.date.toString();
    });
    return res;
  }(),
}

var Bands = {
  total       : db.bands.count(),
  notracks    : db.bands.count({tracks: []}),
  nothumb     : db.bands.count({thumbnail: false}),
  norelated   : db.bands.count({related: []}),
}

print("EVENTS\nCount: " + db.events.count());

print(
  "LIVE EVENTS\n",
  "|-> count\t\t", Live.total, "\n",
  "|-> missing\t", Live.missing, perc(Live.missing, Events.count), "\n",
  "|-> missing ids", Live.missingIds.join(","), "\n",
  "|-> festival\t", Live.festival, perc(Live.festival, Live.total), "\n",
  "|-> past\t\t", Live.past, perc(Live.past, Live.total), "\n",
  "|-> awesome\t", Live.awesome, perc(Live.awesome, Live.total), "\n",
  "|-> crap\t\t", Live.crap, perc(Live.crap, Live.total), "\n",
  "|-> first\t\t", Live.firstDate, "\n",
  "|-> latest\t\t", Live.latestDate
);

print(
  "BANDS\n",
  "|-> count\t\t\t", Bands.total, "\n",
  "|-> no tracks\t\t", Bands.notracks, perc(Bands.notracks, Bands.total), "\n",
  "|-> no thumbnail\t", Bands.nothumb, perc(Bands.nothumb, Bands.total), "\n",
  "|-> no related\t\t", Bands.norelated, perc(Bands.norelated, Bands.total), "\n"

);
